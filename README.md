# Warstwa danych systemu RSO-BIK #

## Instalacja ##

### Potrzebne programy: ###

* [docker-compose](https://docs.docker.com/compose/install/)

### Skrypt instalacyjny: ###


```
#!sh
$ ./install.sh

```

## Uruchamianie ##

Stawianie serwera przy pomocy skryptu:

```
#!sh
$ ./run.sh
```

Aplikacja stoi pod adresem: 
```
#!sh
$ 127.0.0.1:8000
```

Podgląd bazy danych pod adresem:
```
#!sh
$ 127.0.0.1:8000/admin
```

```
#!sh
user: root
hasło: 1qazxsw2
```

## Usuwanie, czyszczenie ##
Sprzątanie wszystkich dockerów, przy pomocy skryptu:
```
#!sh
$ ./clearall.sh
```

## Bibliografia ##
* [django-compose](https://docs.docker.com/compose/django/)
* [remove django containers](https://coderwall.com/p/ewk0mq/stop-remove-all-docker-containers)
