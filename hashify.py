import hashlib
from rso.models import ChangeLogElement

for cl in ChangeLogElement.objects.all():
    s = hashlib.md5(str(cl.pk))
    cl.my_hash = s.hexdigest()
    cl.save()
