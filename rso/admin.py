#-*- coding: utf-8 -*-
from django.contrib import admin
from django import forms

from .models import Customer, Credit, Node, Payment, ChangeLogElement, \
    QuestQueueElement


class NodeAdmin(admin.ModelAdmin):
    model = Node
    list_display = ('ip', 'alive')


class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    list_display = ('__unicode__', 'first_name', 'last_name', 'pesel', 
                    'id_card_number', 'phone_number', 'email',
                    'address', 'publication_date')


class CreditAdmin(admin.ModelAdmin):
    model = Credit
    list_display = ('__unicode__', 'customer', 'name', 'value')


class PaymentAdmin(admin.ModelAdmin):
    model = Payment
    list_display = ('__unicode__', 'credit', 'value', 'publication_date')


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Credit, CreditAdmin)
admin.site.register(Node, NodeAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(ChangeLogElement)
admin.site.register(QuestQueueElement)
