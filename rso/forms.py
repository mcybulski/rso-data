#-*- coding: utf-8 -*-
from django import forms

from .models import Credit, Payment


class CreditForm(forms.Form):
    class Meta:
        model = Credit


class PaymentForm(forms.Form):
    class Meta:
        model = Payment
