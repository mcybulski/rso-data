# -*- coding: utf-8 -*-
DEBUG = True

HOST = 'http://ppeczek.prez.es'

ALLOWED_HOSTS = ['ppeczek.prez.es']

ADMINS = (
    ('Piotr Pęczek', 'ppeczek@gmail.com'),
)
