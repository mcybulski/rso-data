# -*- coding: utf-8 -*-
import json
import socket
from time import mktime
import urllib, urllib2

from django.core.management.base import BaseCommand
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse

from rso.models import Node, ChangeLogElement, QuestQueueElement, \
    GATH, BRDC, REPL, \
    NEW_CREDIT, NEW_PAYMENT


class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%dT%H:%M:%SZ')
        elif isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        task = QuestQueueElement.objects.get_first()
        # nodes = Node.objects.get_all_from_id(task.last_id)
        nodes = Node.objects.all()
        my_ip = socket.gethostbyname(socket.gethostname())
        print my_ip
        print nodes
        #TODO jesli node bedzie odciety od sieci niech krzyczy
        if task.action_type == BRDC:
            print "BRDC"
            for node in nodes:
                url = "http://" + node.ip + ":8000" + node.get_broadcast_url()
                data = task.changelog
                try:
                    print url
                    request = urllib2.Request(url)
                    request.add_header('Content-Type', 'application/json')
                    response = urllib2.urlopen(request, data, 5)
                except urllib2.URLError, e:
                    print e
                except socket.timeout:
                    print "Timed out!"
                task.last_id = node.pk
                task.save()

        elif task.action_type == REPL:
            cl = task.log
            sdata = json.loads(task.log.json_task)
            sdata.update({
                'changelog': {
                    'author_ip': cl.author_ip,
                    'timestamp': cl.timestamp,
                    'change_type': cl.change_type,
                    'my_hash': cl.my_hash
                },
            })
            data = json.dumps(sdata, cls=DjangoJSONEncoder).encode('utf-8')
            # print data
            for node in nodes:
                # print node
                if task.object_type == NEW_CREDIT:
                    url = "http://" + node.ip + ":8000" + node.get_new_credit_url()
                if task.object_type == NEW_PAYMENT:
                    url = "http://" + node.ip + ":8000" + node.get_new_payment_url()
                # print url
                # print data
                try:
                    request = urllib2.Request(url)
                    request.add_header('Content-Type', 'application/json')
                    response = urllib2.urlopen(request, data, 5)
                    print response
                except urllib2.URLError, e:
                    print e
                except socket.timeout:
                    print "Timed out!"
                task.last_id = node.pk
                task.save()
        task.delete()

