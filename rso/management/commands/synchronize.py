# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from rso.models import ChangeLogElement, QuestQueueElement, GATH, BRDC


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        cl = ChangeLogElement.objects.dump_json()
        q = QuestQueueElement(action_type=BRDC, changelog=cl)
        q.save()
