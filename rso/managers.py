#-*- coding: utf-8 -*-
import json
import socket

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Sum


class QuestQueueElementManager(models.Manager):
    def get_first(self):
        return self.get_query_set().order_by('added')[:1][0]


class ChangeLogElementManager(models.Manager):
    def dump_json(self):
        cl = self.get_query_set().all()
        if len(cl) > 0:
            result = []
            for c in cl:
                try:
                    request = json.loads(c.json_task)
                except Exception:
                    request = None

                result.append({
                    'author_ip': c.author_ip,
                    'my_hash': c.my_hash,
                    'timestamp': c.timestamp,
                    'request': request,
                    'type': c.change_type
                })
            return json.dumps(result, cls=DjangoJSONEncoder)
        return None


class CreditManager(models.Manager):
    def get_payment_credit(self, payment_pk):
        return self.get_query_set().get(pk=payment_pk)

    def get_customer_credits(self, customer_pk):
        return self.get_query_set().filter(customer__pk=customer_pk).\
            aggregate(Sum('value'))


class CustomerManager(models.Manager):
    def get_customer(self, pesel):
        return self.get_query_set().get(pesel=pesel)


class NodeManager(models.Manager):
    def get_alive(self, serv_name):
        my_ip = socket.gethostbyname(serv_name)
        print my_ip
        return self.get_query_set().filter(alive=True).exclude(ip=my_ip)

    def get_all_from_id(self, serv_name, from_id):
        my_ip = socket.gethostbyname(serv_name)
        print my_ip
        return self.get_query_set().filter(pk__gt=from_id).exclude(ip=my_ip).\
            order_by('id')


class PaymentManager(models.Manager):
    def get_aggregated(self, credit_pk):
        return self.get_query_set().filter(credit__pk=credit_pk).\
                aggregate(Sum('value'))

    def get_customer_payments(self, customer_pk):
        return self.get_query_set().\
            filter(credit__customer__pk=customer_pk).\
            aggregate(Sum('value'))
