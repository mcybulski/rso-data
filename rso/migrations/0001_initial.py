# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Customer'
        db.create_table(u'rso_customer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('pesel', self.gf('django.db.models.fields.CharField')(max_length=11)),
            ('id_card_number', self.gf('django.db.models.fields.CharField')(max_length=9)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'rso', ['Customer'])

        # Adding model 'Credit'
        db.create_table(u'rso_credit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('value', self.gf('django.db.models.fields.DecimalField')(default='1', max_digits=8, decimal_places=4)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.Customer'])),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'rso', ['Credit'])

        # Adding model 'Payment'
        db.create_table(u'rso_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('value', self.gf('django.db.models.fields.DecimalField')(default='1', max_digits=8, decimal_places=4)),
            ('credit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.Credit'])),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.Customer'])),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'rso', ['Payment'])


    def backwards(self, orm):
        # Deleting model 'Customer'
        db.delete_table(u'rso_customer')

        # Deleting model 'Credit'
        db.delete_table(u'rso_credit')

        # Deleting model 'Payment'
        db.delete_table(u'rso_payment')


    models = {
        u'rso.credit': {
            'Meta': {'object_name': 'Credit'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.customer': {
            'Meta': {'object_name': 'Customer'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_card_number': ('django.db.models.fields.CharField', [], {'max_length': '9'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.payment': {
            'Meta': {'object_name': 'Payment'},
            'credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']"}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        }
    }

    complete_apps = ['rso']