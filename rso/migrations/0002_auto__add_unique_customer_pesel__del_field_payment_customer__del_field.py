# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Customer', fields ['pesel']
        db.create_unique(u'rso_customer', ['pesel'])

        # Deleting field 'Payment.customer'
        db.delete_column(u'rso_payment', 'customer_id')

        # Deleting field 'Payment.name'
        db.delete_column(u'rso_payment', 'name')


    def backwards(self, orm):
        # Removing unique constraint on 'Customer', fields ['pesel']
        db.delete_unique(u'rso_customer', ['pesel'])


        # User chose to not deal with backwards NULL issues for 'Payment.customer'
        raise RuntimeError("Cannot reverse this migration. 'Payment.customer' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Payment.name'
        raise RuntimeError("Cannot reverse this migration. 'Payment.name' and its values cannot be restored.")

    models = {
        u'rso.credit': {
            'Meta': {'object_name': 'Credit'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.customer': {
            'Meta': {'ordering': "['last_name', 'first_name', 'pesel']", 'object_name': 'Customer'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_card_number': ('django.db.models.fields.CharField', [], {'max_length': '9'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'pesel': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '11'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.payment': {
            'Meta': {'object_name': 'Payment'},
            'credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        }
    }

    complete_apps = ['rso']