# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChangeLog'
        db.create_table(u'rso_changelog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author_ip', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('change_type', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal(u'rso', ['ChangeLog'])


    def backwards(self, orm):
        # Deleting model 'ChangeLog'
        db.delete_table(u'rso_changelog')


    models = {
        u'rso.changelog': {
            'Meta': {'object_name': 'ChangeLog'},
            'author_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'change_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.credit': {
            'Meta': {'object_name': 'Credit'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.customer': {
            'Meta': {'ordering': "['last_name', 'first_name', 'pesel']", 'object_name': 'Customer'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_card_number': ('django.db.models.fields.CharField', [], {'max_length': '9'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'pesel': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '11'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.node': {
            'Meta': {'object_name': 'Node'},
            'alive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'})
        },
        u'rso.payment': {
            'Meta': {'object_name': 'Payment'},
            'credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        }
    }

    complete_apps = ['rso']