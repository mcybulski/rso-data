# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ChangeLog'
        db.delete_table(u'rso_changelog')

        # Adding model 'ChangeLogElement'
        db.create_table(u'rso_changelogelement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author_ip', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('change_type', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('my_hash', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'rso', ['ChangeLogElement'])

        # Adding model 'QuestQueueElement'
        db.create_table(u'rso_questqueueelement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object_type', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('action_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('send_credit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.Credit'], null=True, blank=True)),
            ('send_payment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.Payment'], null=True, blank=True)),
            ('last_id', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rso.ChangeLogElement'], null=True, blank=True)),
            ('changelog', self.gf('jsonfield.fields.JSONField')()),
        ))
        db.send_create_signal(u'rso', ['QuestQueueElement'])


    def backwards(self, orm):
        # Adding model 'ChangeLog'
        db.create_table(u'rso_changelog', (
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('change_type', self.gf('django.db.models.fields.CharField')(max_length=3)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author_ip', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39)),
        ))
        db.send_create_signal(u'rso', ['ChangeLog'])

        # Deleting model 'ChangeLogElement'
        db.delete_table(u'rso_changelogelement')

        # Deleting model 'QuestQueueElement'
        db.delete_table(u'rso_questqueueelement')


    models = {
        u'rso.changelogelement': {
            'Meta': {'object_name': 'ChangeLogElement'},
            'author_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'change_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'my_hash': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.credit': {
            'Meta': {'object_name': 'Credit'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.customer': {
            'Meta': {'ordering': "['last_name', 'first_name', 'pesel']", 'object_name': 'Customer'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_card_number': ('django.db.models.fields.CharField', [], {'max_length': '9'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'pesel': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '11'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.node': {
            'Meta': {'object_name': 'Node'},
            'alive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'})
        },
        u'rso.payment': {
            'Meta': {'object_name': 'Payment'},
            'credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.questqueueelement': {
            'Meta': {'object_name': 'QuestQueueElement'},
            'action_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'changelog': ('jsonfield.fields.JSONField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_id': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.ChangeLogElement']", 'null': 'True', 'blank': 'True'}),
            'object_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'send_credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']", 'null': 'True', 'blank': 'True'}),
            'send_payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Payment']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['rso']