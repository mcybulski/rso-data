# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Customer.first_name'
        db.alter_column(u'rso_customer', 'first_name', self.gf('django.db.models.fields.CharField')(max_length=256))

        # Changing field 'Customer.last_name'
        db.alter_column(u'rso_customer', 'last_name', self.gf('django.db.models.fields.CharField')(max_length=256))

        # Changing field 'Customer.email'
        db.alter_column(u'rso_customer', 'email', self.gf('django.db.models.fields.CharField')(max_length=256))

        # Changing field 'Credit.name'
        db.alter_column(u'rso_credit', 'name', self.gf('django.db.models.fields.CharField')(max_length=256))

        # Changing field 'Credit.value'
        db.alter_column(u'rso_credit', 'value', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=4))
        # Deleting field 'QuestQueueElement.from_ip'
        db.delete_column(u'rso_questqueueelement', 'from_ip')


    def backwards(self, orm):

        # Changing field 'Customer.first_name'
        db.alter_column(u'rso_customer', 'first_name', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Customer.last_name'
        db.alter_column(u'rso_customer', 'last_name', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Customer.email'
        db.alter_column(u'rso_customer', 'email', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Credit.name'
        db.alter_column(u'rso_credit', 'name', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Credit.value'
        db.alter_column(u'rso_credit', 'value', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=4))
        # Adding field 'QuestQueueElement.from_ip'
        db.add_column(u'rso_questqueueelement', 'from_ip',
                      self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39, null=True, blank=True),
                      keep_default=False)


    models = {
        u'rso.changelogelement': {
            'Meta': {'object_name': 'ChangeLogElement'},
            'author_ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'}),
            'change_type': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json_task': ('jsonfield.fields.JSONField', [], {}),
            'my_hash': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.credit': {
            'Meta': {'object_name': 'Credit'},
            'bank_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '15', 'decimal_places': '4'})
        },
        u'rso.customer': {
            'Meta': {'ordering': "['last_name', 'first_name', 'pesel']", 'object_name': 'Customer'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_card_number': ('django.db.models.fields.CharField', [], {'max_length': '9'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'pesel': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '11'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'rso.node': {
            'Meta': {'object_name': 'Node'},
            'alive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39'})
        },
        u'rso.payment': {
            'Meta': {'object_name': 'Payment'},
            'credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '8', 'decimal_places': '4'})
        },
        u'rso.questqueueelement': {
            'Meta': {'object_name': 'QuestQueueElement'},
            'action_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'changelog': ('jsonfield.fields.JSONField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_id': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.ChangeLogElement']", 'null': 'True', 'blank': 'True'}),
            'object_type': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'send_credit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Credit']", 'null': 'True', 'blank': 'True'}),
            'send_payment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rso.Payment']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['rso']