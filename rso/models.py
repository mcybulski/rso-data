#-*- coding: utf-8 -*-
import datetime
from decimal import Decimal
from jsonfield import JSONField
import time
from django.core.urlresolvers import reverse
from django.core.validators import validate_ipv4_address
from django.db import models
from django.utils.translation import ugettext as _

from .managers import CreditManager, CustomerManager, NodeManager, \
    PaymentManager, ChangeLogElementManager, QuestQueueElementManager


NEW_CREDIT = 'NCR'
NEW_PAYMENT = 'NPA'

# SYNC = 'S'
GATH = 'G'
BRDC = 'B'
REPL = 'R'

class Node(models.Model):
    alive = models.BooleanField(_(u'alive'), default=False)
    ip = models.GenericIPAddressField()

    objects = NodeManager()

    def __unicode__(self):
        return u"%s" % self.ip

    def get_broadcast_url(self):
        return reverse('broadcast')

    def get_new_credit_url(self):
        return reverse('data-new-credit')

    def get_new_payment_url(self):
        return reverse('data-new-payment')


class ChangeLogElement(models.Model):
    author_ip = models.GenericIPAddressField()
    timestamp = models.DateTimeField(auto_now_add=True)
    CHANGE_TYPES = (
        (NEW_CREDIT, u'New credit'),
        (NEW_PAYMENT, u'New payment'),
    )
    change_type = models.CharField(max_length=3,
                                   choices=CHANGE_TYPES)
    my_hash = models.CharField(max_length=128)
    json_task = JSONField()

    objects = ChangeLogElementManager()

    def __unicode__(self):
        return u"%s: %s - %s" % (self.author_ip, self.timestamp,
                                 self.change_type)


class QuestQueueElement(models.Model):
    object_type = models.CharField(max_length=3,
                                   choices=ChangeLogElement.CHANGE_TYPES,
                                   blank=True, null=True)
    ACTION_TYPES = (
        (GATH, u'Synchronize-1'),
        (BRDC, u'Synchronize-2'),
        (REPL, u'Replicate'),
    )
    action_type = models.CharField(max_length=1,
                                   choices=ACTION_TYPES)
    send_credit = models.ForeignKey('Credit', verbose_name=_(u'credit'),
                                    blank=True, null=True)
    send_payment = models.ForeignKey('Payment', verbose_name=_(u'payment'),
                                     blank=True, null=True)
    last_id = models.BigIntegerField('last node id', default=0)
    '''Pojedynczy wpis changeloga'''
    log = models.ForeignKey(ChangeLogElement, verbose_name=_(u'log'),
                            blank=True, null=True)
    added = models.DateTimeField(verbose_name=_('added'), auto_now_add=True)
    '''To caly zrzut changeloga'''
    changelog = JSONField()

    objects = QuestQueueElementManager()

    def __unicode__(self):
        return u"%s: %s" % (self.action_type, self.added)


class Customer(models.Model):
    first_name = models.CharField(_(u'first name'), max_length=256)
    last_name = models.CharField(_(u'last name'), max_length=256)
    pesel = models.CharField(_(u'pesel'), max_length=11, unique=True)
    id_card_number = models.CharField(_(u'id card number'), max_length=9)
    phone_number =  models.CharField(_(u'phone number'), max_length=20)
    email = models.CharField(_(u'email'), max_length=256)
    address = models.TextField(_(u'address'))
    publication_date = models.DateTimeField(
        verbose_name=_(u'publication date'), auto_now_add=True)

    objects = CustomerManager()

    def __unicode__(self):
        return u"%s %s (%s)" % (self.first_name, self.last_name, self.pesel)

    def get_name(self):
        return u"%s %s" % (self.first_name, self.last_name)

    class Meta:
        ordering = ['last_name', 'first_name', 'pesel']
        verbose_name = 'customer'
        verbose_name_plural = 'customers'


class Credit(models.Model):
    name = models.CharField(_(u'first name'), max_length=256)
    value = models.DecimalField(max_digits=15, decimal_places=4,
        default=Decimal(1.00))
    customer = models.ForeignKey(Customer, verbose_name=_(u'customer'))
    publication_date = models.DateTimeField(
        verbose_name=_(u'publication date'), auto_now_add=True)
    bank_id = models.BigIntegerField()

    objects = CreditManager()

    def __unicode__(self):
        return u"%s - %s" % (self.customer, self.name)

    class Meta:
        verbose_name = 'credit'
        verbose_name_plural = 'credits'


class Payment(models.Model):
    value = models.DecimalField(max_digits=15, decimal_places=4,
        default=Decimal(1.00))
    credit = models.ForeignKey(Credit, verbose_name=_(u'credit'))
    publication_date = models.DateTimeField(
        verbose_name=_(u'publication date'), auto_now_add=True)

    objects = PaymentManager()

    def __unicode__(self):
        return u"%s" % (self.credit)

    class Meta:
        verbose_name = 'payment'
        verbose_name_plural = 'payments'
