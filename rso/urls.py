from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.decorators.csrf import csrf_exempt

from .views import MonthlyObligationsView, CreditCreateView, PaymentCreateView,\
    BroadcastDataView, CreditReplicateView, PaymentReplicateView


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^app/obligations/(?P<pesel>\w+)/$', MonthlyObligationsView.as_view(),
        name='app-obligations'),
    url(r'^app/obligations/$', csrf_exempt(CreditCreateView.as_view()),
        name='app-new-credit'),
    url(r'^app/new-payment/(?P<pesel>\w+)/$',
        csrf_exempt(PaymentCreateView.as_view()),
        name='app-new-payment'),
    url(r'^data/broadcast-data/$',
        csrf_exempt(BroadcastDataView.as_view()),
        name='broadcast'),
    url(r'^data/obligations/$', csrf_exempt(CreditReplicateView.as_view()),
        name='data-new-credit'),
    url(r'^data/new-payment/$', csrf_exempt(PaymentReplicateView.as_view()),
        name='data-new-payment'),
)

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()

# Keep this at the bottom of the URL definitions
urlpatterns += patterns(
    '',
)
    # url(r'', include('pages.urls')))
