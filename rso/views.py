# -*- coding: utf-8 -*-
import datetime
import json
import socket

from django import template
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpRequest, HttpResponse, Http404
from django.views.generic import CreateView, View

from .models import QuestQueueElement, ChangeLogElement, Customer, Credit, \
    Node, Payment, \
    NEW_CREDIT, NEW_PAYMENT, REPL, GATH


ADDR_NAME = 'REMOTE_ADDR'
# ADDR_NAME = 'HTTP_X_FORWARDED_FOR'

class JSONMixin(object):
    def render_to_json_response(self, context, **httpresponse_kwargs):
        return self.get_json_response(
            self.convert_context_to_json(context),
            **httpresponse_kwargs
        )

    def render_to_response(self, context, **httpresponse_kwargs):
        return self.get_json_response(
            json.dumps(context),
            **httpresponse_kwargs
        )

    def get_json_response(self, content, **httpresponse_kwargs):
        return HttpResponse(
            content,
            content_type='application/json',
            **httpresponse_kwargs
        )


class BroadcastDataView(JSONMixin, View):
    def dispatch(self, request, *args, **kwargs):
        client_address = request.META.get(ADDR_NAME)
        print client_address
        # client_node = self.get_client(client_address)
        my_ip = socket.gethostbyname(request.META['SERVER_NAME'])
        print my_ip
        return super(BroadcastDataView, self).dispatch(request, *args, **kwargs)

    def get_client(self, ip):
        try:
            client = Node.objects.get(ip=ip)
        except Exception:
            raise PermissionDenied()
        return client

    def post(self, request, *args, **kwargs):
        received_data = json.loads(self.request.raw_post_data)
        client_address = request.META.get(ADDR_NAME)
        print client_address
        print received_data
        to_get = []
        for cl in received_data:
            my_cls = ChangeLogElement.objects.filter(author_ip=cl['author_ip'],
                                                     my_hash=cl['my_hash'])
            if len(my_cls) == 0:
                print cl
                if cl['type'] == NEW_CREDIT:
                    customer, p = Customer.objects.get_or_create(
                        first_name = cl['request']['client']['firstName'],
                        last_name = cl['request']['client']['lastName'],
                        pesel = cl['request']['client']['pesel'],
                        id_card_number = cl['request']['client']['documentNumber'],
                        phone_number = cl['request']['client']['phoneNumber'],
                        email = cl['request']['client']['email'],
                        address = cl['request']['client']['address']
                    )
                    credit = Credit(
                        name = cl['request']['credit']['name'],
                        value = cl['request']['credit']['value'],
                        customer = customer,
                        bank_id = cl['request']['uid']
                    )
                elif cl['type'] == NEW_PAYMENT:
                    try:
                        credit = Credit.objects.get(pk=int(cl['request']['payment']['creditId']),
                                                    bank_id=int(cl['request']['uid']))
                        payment = Payment(
                            credit = credit,
                            value = cl['request']['payment']['value'],
                        )
                        payment.save()
                    except:
                        print "byl probl"
                c = ChangeLogElement(author_ip=cl['author_ip'],change_type=cl['type'],
                                     my_hash=cl['my_hash'],timestamp=cl['timestamp'],
                                     json_task=cl['request'])
                c.save()
        return self.render_to_response({})

    def render_to_reponse(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class MonthlyObligationsView(JSONMixin, View):
    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_object(self):
        return Customer.objects.get(pesel=self.kwargs['pesel'])

    def get_context_data(self, **kwargs):
        try:
            customer = self.get_object()
        except Exception:
            return {
                'value': None,
                'errorCode': 1
            }
        credits = Credit.objects.get_customer_credits(customer.pk)
        payments = Payment.objects.get_customer_payments(customer.pk)
        credit_val = 0
        if credits['value__sum'] is not None:
            if payments['value__sum'] is not None:
                credit_val = payments['value__sum']
            context = {
                'value': int(credits['value__sum'] - credit_val),
                'errorCode': None
            }
        else:
            context = {
                'value': None,
                'errorCode': 1
            }
        return context

    def render_to_reponse(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class CreditCreateView(JSONMixin, CreateView):
    model = Credit

    # def get_context_data(self, **kwargs):
        # context = super(CreditCreateView, self).get_context_data(**kwargs)
        # context.update({
        # return context

    def post(self, request, *args, **kwargs):
        received_data = json.loads(self.request.raw_post_data)
        my_ip = socket.gethostbyname(request.META['SERVER_NAME'])
        print my_ip
        context = {
            'creditId': None,
            'errorCode': 1
        }
        try:
            customer, p = Customer.objects.get_or_create(
                first_name = received_data['client']['firstName'],
                last_name = received_data['client']['lastName'],
                pesel = received_data['client']['pesel'],
                id_card_number = received_data['client']['documentNumber'],
                phone_number = received_data['client']['phoneNumber'],
                email = received_data['client']['email'],
                address = received_data['client']['address']
            )
        except Exception:
            return self.render_to_response(context)
        credit = Credit(
            name = received_data['credit']['name'],
            value = received_data['credit']['value'],
            customer = customer,
            bank_id = received_data['uid']
        )
        credit.save()
        #TODO czy tu nie try except?
        context.update({
            'creditId': credit.pk,
            'errorCode': None
        })
        changelog = ChangeLogElement(author_ip=my_ip, change_type=NEW_CREDIT,
                                     my_hash=received_data['hash'],
                                     json_task=self.request.raw_post_data)
        changelog.save()
        quest = QuestQueueElement(object_type=NEW_CREDIT, action_type=REPL,
                                    send_credit=credit, log=changelog)
        quest.save()

        return self.render_to_response(context)

    def render_to_reponse(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class CreditReplicateView(JSONMixin, CreateView):
    model = Credit

    def dispatch(self, request, *args, **kwargs):
        client_address = request.META.get(ADDR_NAME)
        print request.raw_post_data
        # client_node = self.get_client(client_address)
        return super(CreditReplicateView, self).dispatch(request, *args,
                                                         **kwargs)

    def get_client(self, ip):
        try:
            client = Node.objects.get(ip=ip)
        except Exception:
            raise PermissionDenied()
        return client

    def post(self, request, *args, **kwargs):
        received_data = json.loads(self.request.raw_post_data)
        author_ip = received_data['changelog']['author_ip']
        timestamp = received_data['changelog']['timestamp']
        change_type = received_data['changelog']['change_type']
        my_hash = received_data['changelog']['my_hash']
        json_task = json.dumps({
            'hash': my_hash,
            'client': received_data['client'],
            'credit': received_data['credit'],
            'uid': received_data['uid']
        })
        cl = ChangeLogElement.objects.filter(author_ip=author_ip,
                                             timestamp=timestamp,
                                             change_type=change_type,
                                             my_hash=my_hash)
        if len(cl) > 0:
            raise Http404('Already exists')

        try:
            customer, p = Customer.objects.get_or_create(
                first_name = received_data['client']['firstName'],
                last_name = received_data['client']['lastName'],
                pesel = received_data['client']['pesel'],
                id_card_number = received_data['client']['documentNumber'],
                phone_number = received_data['client']['phoneNumber'],
                email = received_data['client']['email'],
                address = received_data['client']['address'],
            )
            credit = Credit(
                name = received_data['credit']['name'],
                value = received_data['credit']['value'],
                customer = customer,
                bank_id = received_data['uid']
            )
            credit.save()
            cl = ChangeLogElement(author_ip=author_ip, timestamp=timestamp,
                                change_type=change_type, my_hash=my_hash,
                                json_task=json_task)
            cl.save()
        except Exception:
            raise Http404('Bad customer')

        return self.render_to_response({})


class PaymentCreateView(JSONMixin, CreateView):
    model = Payment

    def post(self, request, *args, **kwargs):
        received_data = json.loads(self.request.raw_post_data)
        my_ip = socket.gethostbyname(request.META['SERVER_NAME'])
        context = {
            'remaining': None,
            'errorCode': None
        }
        print received_data
        try:
            credit = Credit.objects.get(pk=received_data['payment']['creditId'],
                                        bank_id=received_data['uid'])
            payment = Payment(
                credit = credit,
                value = received_data['payment']['value'],
            )
            payment.save()
            changelog = ChangeLogElement(author_ip=my_ip, change_type=NEW_PAYMENT,
                                        my_hash=received_data['hash'],
                                        json_task=self.request.raw_post_data)
            changelog.save()
            quest = QuestQueueElement(object_type=NEW_PAYMENT, action_type=REPL,
                                        send_payment=payment, log=changelog)
            quest.save()
            payments = Payment.objects.get_aggregated(credit.pk)
            context['remaining'] = int(credit.value - payments['value__sum'])
        except Exception:
            context['errorCode'] = 1

        return self.render_to_response(context)

    def render_to_reponse(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)


class PaymentReplicateView(JSONMixin, CreateView):
    model = Payment

    def dispatch(self, request, *args, **kwargs):
        client_address = request.META.get(ADDR_NAME)
        # client_node = self.get_client(client_address)
        return super(PaymentReplicateView, self).dispatch(request, *args,
                                                         **kwargs)

    def get_client(self, ip):
        try:
            client = Node.objects.get(ip=ip)
        except Exception:
            raise PermissionDenied()
        return client

    def post(self, request, *args, **kwargs):
        received_data = json.loads(self.request.raw_post_data)
        author_ip = received_data['changelog']['author_ip']
        timestamp = received_data['changelog']['timestamp']
        change_type = received_data['changelog']['change_type']
        my_hash = received_data['changelog']['my_hash']
        cl = ChangeLogElement.objects.filter(author_ip=author_ip,
                                             timestamp=timestamp,
                                             change_type=change_type,
                                             my_hash=my_hash)
        if len(cl) > 0:
            raise Http404('Already exists')
        print received_data
        json_task = json.dumps({
            'payment': received_data['payment'],
            'hash': my_hash,
            'uid': received_data['uid']
        })

        try:
            credit = Credit.objects.get(pk=received_data['payment']['creditId'],
                                        bank_id=received_data['uid'])
            payment = Payment(
                credit = credit,
                value = received_data['payment']['value'],
            )
            payment.save()
            cl = ChangeLogElement(author_ip=author_ip, timestamp=timestamp,
                                change_type=change_type, my_hash=my_hash,
                                json_task=json_task)
            cl.save()
        except:
            raise Http404('No credit')

        return self.render_to_response({})

    def render_to_reponse(self, context, **response_kwargs):
        return self.render_to_json_response(context, **response_kwargs)
